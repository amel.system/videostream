package com.amelsystem.videostream;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;

import com.amelsystem.videostream.InterfaceModels.InterfaceJsonObject;
import com.amelsystem.videostream.Tools.ApiTool;

import org.json.JSONObject;

public class SigninActivity extends AppCompatActivity implements InterfaceJsonObject {

    private AppCompatButton btnLogin = null;
    private AppCompatTextView btnLoginWithPhone = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);

        Cast();

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                ApiTool apiTool = new ApiTool(SigninActivity.this);
                JSONObject jsonObject = new JSONObject();
                //jsonObject.put("phone" ,"value is here");
                apiTool.sendJsonRequest(jsonObject , "url is here" ,2000,SigninActivity.this);


            }
        });

        btnLoginWithPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SigninActivity.this, SigninPhoneActivity.class);
                startActivity(intent);
            }
        });

    }

    private void Cast() {

        btnLogin = findViewById(R.id.btnRecovery);

        btnLoginWithPhone = findViewById(R.id.tvPhoneNumber);


    }

    @Override
    public void jsonRecieved(boolean result, JSONObject jsonObject) {

        Intent intent = new Intent(SigninActivity.this , MainActivity.class);
        startActivity(intent);
    }
}