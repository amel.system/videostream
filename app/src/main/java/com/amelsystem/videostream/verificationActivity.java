package com.amelsystem.videostream;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class verificationActivity extends AppCompatActivity {

    private AppCompatButton btnVerification = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);

        btnVerification = findViewById(R.id.btnRecovery);

        btnVerification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(verificationActivity.this , MainActivity.class);
                startActivity(intent);
            }
        });

    }
}
