package com.amelsystem.videostream;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.amelsystem.videostream.InterfaceModels.InterfaceJsonObject;
import com.amelsystem.videostream.Tools.ApiTool;

import org.json.JSONObject;

public class SigninPhoneActivity extends AppCompatActivity implements InterfaceJsonObject {

    private AppCompatTextView btnSigninWithEmail = null;
    private AppCompatButton btnLogin = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin_phone);

        btnSigninWithEmail = findViewById(R.id.tvEmail);
        btnLogin = findViewById(R.id.btnRecovery);


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ApiTool apiTool = new ApiTool(SigninPhoneActivity.this);
                JSONObject jsonObject = new JSONObject();
                //jsonObject.put("phone" ,"value is here");
                apiTool.sendJsonRequest(jsonObject , "url is here" ,2000,SigninPhoneActivity.this);


            }
        });

        btnSigninWithEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    @Override
    public void jsonRecieved(boolean result, JSONObject jsonObject) {
        //analayze answer here
        Intent intent = new Intent(SigninPhoneActivity.this,verificationActivity.class);
        startActivity(intent);
    }

}
