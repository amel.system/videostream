package com.amelsystem.videostream;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.amelsystem.videostream.Adapter.ItemAdapter;

public class FragmentHome extends Fragment {

    private View view = null;

    private ItemAdapter categoryAdapter = null;
    private ItemAdapter liveAdapter = null;
    private ItemAdapter todayAdapter = null;
    private ItemAdapter mostviewAdapter = null;
//    private ItemAdapter LiveAdapter= null;
//    private ItemAdapter todayAdapter= null;
//    private ItemAdapter Adapter= null;

    RecyclerView recyclerViewCategory = null;


    RecyclerView recyclerViewLive = null;

    RecyclerView recyclerViewToday = null;

    RecyclerView recyclerViewMostView = null;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_home, container, false);


        categoryAdapter = new ItemAdapter();
        liveAdapter = new ItemAdapter();
        todayAdapter = new ItemAdapter();
        mostviewAdapter = new ItemAdapter();


        recyclerViewCategory = view.findViewById(R.id.recyclerViewCategory);
        recyclerViewLive = view.findViewById(R.id.recyclerViewLiveNow);
        recyclerViewToday = view.findViewById(R.id.recyclerViewTopToday);
        recyclerViewMostView = view.findViewById(R.id.recyclerViewMostView);

        RecyclerView.LayoutManager layoutManagerCategory = new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false);
        RecyclerView.LayoutManager layoutManagerLiveNow = new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false);
        RecyclerView.LayoutManager layoutManagerToday = new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false);
        RecyclerView.LayoutManager layoutManagermostView = new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false);

        recyclerViewCategory.setLayoutManager(layoutManagerCategory);
        recyclerViewCategory.setItemAnimator(new DefaultItemAnimator());
        recyclerViewCategory.setAdapter(categoryAdapter);

        recyclerViewLive.setLayoutManager(layoutManagerLiveNow);
        recyclerViewLive.setItemAnimator(new DefaultItemAnimator());
        recyclerViewLive.setAdapter(liveAdapter);

        recyclerViewToday.setLayoutManager(layoutManagerToday);
        recyclerViewToday.setItemAnimator(new DefaultItemAnimator());
        recyclerViewToday.setAdapter(todayAdapter);

        recyclerViewMostView.setLayoutManager(layoutManagermostView);
        recyclerViewMostView.setItemAnimator(new DefaultItemAnimator());
        recyclerViewMostView.setAdapter(mostviewAdapter);

        categoryAdapter.notifyDataSetChanged();
        liveAdapter.notifyDataSetChanged();
        todayAdapter.notifyDataSetChanged();
        mostviewAdapter.notifyDataSetChanged();


        return view;


    }
}
