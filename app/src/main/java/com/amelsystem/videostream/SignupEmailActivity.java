package com.amelsystem.videostream;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class SignupEmailActivity extends AppCompatActivity {

    private AppCompatTextView tvPhoneNumber = null;
    private AppCompatButton btnLoginPhone = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_email);
        Cast();

        tvPhoneNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignupEmailActivity.this,SigninPhoneActivity.class);
                startActivity(intent);
                finish();
            }
        });

//        btnLoginPhone.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(SignupEmailActivity.this,SigninPhoneActivity.class);
//                startActivity(intent);
//            }
//        });

    }

    private void Cast() {

        tvPhoneNumber = findViewById(R.id.tvPhoneNumber);
        btnLoginPhone = findViewById(R.id.btnForgetPassword);
    }
}
