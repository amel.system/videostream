package com.amelsystem.videostream;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {

    AppCompatImageButton btnMenu = null;
    AppCompatImageButton btnSearch = null;
    DrawerLayout drawerLayout = null;

    BottomNavigationView bottomNavigationView = null;


    //fragments
    FragmentHome fragmentHome = null;
    FragmentFavorite fragmentFavorite = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Cast();

        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(GravityCompat.START);
            }
        });

        bottomNavigationView.setOnNavigationItemReselectedListener(new BottomNavigationView.OnNavigationItemReselectedListener() {
            @Override
            public void onNavigationItemReselected(@NonNull MenuItem menuItem) {
                switch (menuItem.getTitle().toString()){

                    case "Home" :
                        loadFragment(fragmentHome);
                        break;

                    case "Favorite" :
                        loadFragment(fragmentFavorite);
                        break;

                    case "Account" :
                        //loadFragment(fragmentHome);
                        break;
                    case "More" :
                        //loadFragment(fragmentHome);
                        break;


                }
            }
        });

    }

    private void  Cast(){

        btnMenu = findViewById(R.id.btnMenu);
        btnSearch = findViewById(R.id.btnSearch);
        drawerLayout = findViewById(R.id.drawerLayoutId);
        bottomNavigationView = findViewById(R.id.bottomNavigationViewId);

        fragmentHome = new FragmentHome();
        fragmentFavorite = new FragmentFavorite();

        loadFragment(fragmentHome);
    }

    private void loadFragment(Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_in_left,R.anim.slide_out_right);
        fragmentTransaction.replace(R.id.frameLayoutFragment, fragment);
        fragmentTransaction.commit();
    }
}
