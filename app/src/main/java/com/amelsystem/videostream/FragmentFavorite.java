package com.amelsystem.videostream;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.amelsystem.videostream.Adapter.ItemAdapter;

public class FragmentFavorite extends Fragment {

    private View view= null;

    private ItemAdapter categoryAdapter = null;

    RecyclerView recyclerViewCategory = null;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_favorite , container,false);

        categoryAdapter = new ItemAdapter();

        recyclerViewCategory = view.findViewById(R.id.recyclerViewCategory);


        RecyclerView.LayoutManager layoutManagerCategory = new GridLayoutManager(getContext(),4);
        recyclerViewCategory.setLayoutManager(layoutManagerCategory);
        recyclerViewCategory.setItemAnimator(new DefaultItemAnimator());
        recyclerViewCategory.setAdapter(categoryAdapter);


        categoryAdapter.notifyDataSetChanged();



        return view;
    }
}
