package com.amelsystem.videostream.Tools;

import android.content.Context;
import android.os.Handler;

import com.amelsystem.videostream.InterfaceModels.InterfaceJsonObject;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

public class ApiTool {

    private Context context;
    private DialogTool dialogTool = null;


    public ApiTool(Context context) {
        this.context = context;
        dialogTool = new DialogTool();
    }

//    public void sendJsonRequest(final JSONObject jsonObject, final String apiUrl, final long time, final InterfaceJsonObject interfaceJsonObject) {
//
//        Runnable runnable = new Runnable() {
//            @Override
//            public void run() {
//                dialogTool.waitingDialog(context);
//            }
//        };
//        runnable.run();
//        new Handler().postDelayed(new Runnable() {
//            public void run() {
//
//                RequestQueue requestQueue = Volley.newRequestQueue(context);
//                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, apiUrl, jsonObject, new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        dialogTool.closeDialog();
//                        interfaceJsonObject.jsonRecieved(true, response);
//                    }
//                }, new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        interfaceJsonObject.jsonRecieved(false, null);
//                        dialogTool.closeDialog();
//                    }
//                });
//
//                requestQueue.add(jsonObjectRequest);
//                requestQueue.start();
//            }
//        }, time);
//
//    }

    public void sendJsonRequest(final JSONObject jsonObject, final String apiUrl, final long time, final InterfaceJsonObject interfaceJsonObject) {

        new Runnable() {
            @Override
            public void run() {
                dialogTool.waitingDialog(context);
            }
        }.run();

        new Handler().postDelayed(new Runnable() {
            public void run() {
                RequestQueue requestQueue = Volley.newRequestQueue(context);
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, apiUrl, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dialogTool.closeDialog();
                        interfaceJsonObject.jsonRecieved(true, response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialogTool.closeDialog();
                        interfaceJsonObject.jsonRecieved(false, null);
                    }
                });
                requestQueue.add(jsonObjectRequest);
                requestQueue.start();
            }
        }, time);

    }


//    public void sendLoginRequest(final JSONObject jsonObject, final String apiUrl, final long time, final InterfaceJsonObject interfaceJsonObject) {
//
//        new Runnable() {
//            @Override
//            public void run() {
//                dialogTool.waitingDialog(context);
//            }
//        }.run();
//
//        new Handler().postDelayed(new Runnable() {
//            public void run() {
//                RequestQueue requestQueue = Volley.newRequestQueue(context);
//                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, apiUrl, jsonObject, new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        dialogTool.closeDialog();
//                        interfaceJsonObject.jsonRecieved(true, response);
//                    }
//                }, new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        dialogTool.closeDialog();
//                        interfaceJsonObject.jsonRecieved(false, null);
//                    }
//                });
//                requestQueue.add(jsonObjectRequest);
//                requestQueue.start();
//            }
//        }, time);
//
//    }

}
