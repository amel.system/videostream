package com.amelsystem.videostream.Tools;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import com.amelsystem.videostream.R;
import com.muddzdev.styleabletoast.StyleableToast;
//import com.emredavarci.noty.Noty;

public class ToastTool {


    public static  void showToastWithImage(Context context , LayoutInflater layoutInflater , String message , int resourceId) {


        // Get the custom layout view.
        View toastView = layoutInflater.inflate(R.layout.layout_toast,null);

        AppCompatTextView tvMessage = toastView.findViewById(R.id.tvMessage);
        tvMessage.setText(message);

        AppCompatImageView ivLogo = toastView.findViewById(R.id.ivLogo);
        ivLogo.setImageResource(resourceId);
        ivLogo.setColorFilter(context.getResources().getColor(R.color.colorSecond));

        // Initiate the Toast instance.
        Toast toast = new Toast(context);
        // Set custom view in toast.
        toast.setView(toastView);
        toast.setDuration(Toast.LENGTH_SHORT);
       // toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();

    }

    public static void showMessageWithImage(Context context , String Message) {

        StyleableToast.makeText(context, Message, Toast.LENGTH_LONG, R.style.mytoast).show();

//        Noty.init(context, Message, rl,
//                Noty.WarningStyle.SIMPLE)
//                .setAnimation(Noty.RevealAnim.SLIDE_UP, Noty.DismissAnim.BACK_TO_BOTTOM, 400, 400)
//                .setWarningInset(0, 0, 0, 0)
//                .setWarningBoxRadius(0, 0, 0, 0)
//                .show();


//        Noty.init(context, Message,rl,
//                Noty.WarningStyle.SIMPLE)
//                .setWarningBoxBgColor("#FFFFFF")
//                .setWarningTappedColor("#CCCCCC")
//                .setWarningTextColor("#232323")
//                .setWarningBoxPosition(Noty.WarningPos.BOTTOM)
//                .show();

    }

}
