package com.amelsystem.videostream.Adapter;

public class CategoryItem {


    private int code = 0;
    private String title = "";
    private String imageUl = "";

    public CategoryItem(int code, String title, String imageUl) {
        this.code = code;
        this.title = title;
        this.imageUl = imageUl;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageUl() {
        return imageUl;
    }

    public void setImageUl(String imageUl) {
        this.imageUl = imageUl;
    }
}
