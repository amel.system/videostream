package com.amelsystem.videostream.Adapter;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.amelsystem.videostream.R;

class CategoryViewHolder extends RecyclerView.ViewHolder {

   // private int code = 0;
    public AppCompatTextView title = null;
    public AppCompatImageView imageUl = null;

    public CategoryViewHolder(@NonNull View itemView) {
        super(itemView);
        title = itemView.findViewById(R.id.tvTitle);
        imageUl = itemView.findViewById(R.id.ivLogo);
    }
}
