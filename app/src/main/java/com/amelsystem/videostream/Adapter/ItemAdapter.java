package com.amelsystem.videostream.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.amelsystem.videostream.R;

import java.util.ArrayList;
import java.util.List;

public class ItemAdapter extends RecyclerView.Adapter<CategoryViewHolder> {

    List<CategoryItem> list;

    public ItemAdapter() {
        list = new ArrayList<>();
        //list.add(new CategoryViewHolder())
        for (int i=0;i<10;i++) {
            list.add(new CategoryItem(i, "item " + String.valueOf(i), "http://logo_1.png"));
        }
    }

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_recycler_category_item, parent, false);
        return new CategoryViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder holder, int position) {

        CategoryItem categoryItem = list.get(position);
        holder.title.setText(categoryItem.getTitle());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
