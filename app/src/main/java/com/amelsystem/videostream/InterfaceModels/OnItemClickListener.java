package com.amelsystem.videostream.InterfaceModels;

public interface OnItemClickListener {

    public void onClick(String title, int code);

}
